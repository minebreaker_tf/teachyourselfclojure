(ns programingclojure.ch1.t1-2)

;p12
(println "hello world")

(defn hello [name] (str "Hello, " name))
(hello "Stu")

;p14
(conj #{} "Stu")

(def visitors (atom #{}))
(swap! visitors conj "Stu")

(println visitors)
(println (deref visitors))
(println @visitors)

(defn hello
    "Writes hello message to *out*. Calls you by username. Knows if you have been here before."
    [username]
    (swap! visitors conj username)
    (str "Hello, " username))

(println (hello "Rich"))
(println @visitors)
