(ns programingclojure.ch1.t1-3)

(require 'clojure.java.io)
(refer 'clojure.java.io)

(use 'clojure.java.io)
(use :reload 'clojure.java.io)

;p18
(use 'clojure.repl)
(doc str)
(find-doc "reduce")

;p19
(source identity)

(println (ancestors (class [1 2 3])))
