(ns programingclojure.t1-1)

;p3
(defn blank? [str]
    (every? #(Character/isWhitespace %) str))

(println (blank? "abc"))
(println (blank? ""))
(println (blank? "   "))

;p4
(defrecord Person [first-name last-name])
(def foo (->Person "Aaron" "Bedra"))
(println foo)

;p7
(defn hello-world [username]
    (println (format "Hello, %s" username)))
(hello-world "world")

;p9
(def accounts (ref #{}))
(defrecord Account [id balance])
(dosync (alter accounts conj (->Account "CLJ" 1000.00)))

;p10
(println (System/getProperties))
(.. "hello" getClass getProtectionDomain)
(.start (new Thread (fn [] (println "Hello" (Thread/currentThread)))))
