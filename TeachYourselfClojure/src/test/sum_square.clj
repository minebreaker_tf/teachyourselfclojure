(ns test.sum-square)

(defn sum-of-squares [seq]
    (reduce (fn [x y] (+ x (* y y))) 0 seq))

(defn sum-of-squares2 [seq]
    (reduce #(+ % (* %2 %2)) 0 seq))

(defn accumulate [combiner, nullValue, l]
    (if (empty? l)
        nullValue
        (combiner
            (first l)
            (accumulate combiner nullValue (nthrest l 1)))))
(defn sum-of-squares3 [seq]
    (accumulate #(+ (* % %) %2) 0 seq))

(println (sum-of-squares '(1 2 3 4 5)))
(println (sum-of-squares2 '(1 2 3 4 5)))
(println (sum-of-squares3 '(1 2 3 4 5)))
