(ns test.fizzbuzz)

(println
    (map
        (fn [x]
            (if (= (rem x 15) 0)
                "fizzbuzz"
                (if (= (rem x 5) 0)
                    "buzz"
                    (if (= (rem x 3) 0)
                        "fizz"
                        x))))
        (range 1 101)))
