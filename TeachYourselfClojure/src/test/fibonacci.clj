(ns test.fibonacci)

(defn fibonacci [i]
    (if (< i 2)
        1
        (+ (fibonacci (- i 1)) (fibonacci (- i 2)))))

(println (fibonacci 5))