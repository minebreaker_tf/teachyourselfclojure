(ns test.euclidean)

(defn euclidean [x y]
    (if (= (rem x y) 0)
        y
        (euclidean y (rem x y))))

(println (euclidean 15 5))
(println (euclidean 4 10))
